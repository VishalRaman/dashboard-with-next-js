import React from "react";
import styles from "./dashboard.module.scss";

export default function Dashboard() {
  return (
    <>
      <div className={`${styles["page-info"]}`}>
        <div className={`${styles["custom-row"]}`}>
          <div className={`${styles["custom-grid"]}`}>
            <div className={`${styles["card"]}`}>
              <h6>Task Name</h6>
              <h4>To-do List</h4>
              <p>Creating To-do list application using HTML, CSS, JavaScript</p>
              <link href="../../todo.js">
                <a>view</a>
              </link>
              <a href="./to-do.html">view</a>
            </div>
          </div>
          <div className={`${styles["custom-grid"]}`}>
            <div className={`${styles["card"]}`}>
              <h6>Task Name</h6>
              <h4>Clock</h4>
              <p>Creating clock application using HTML, CSS, JavaScript</p>
              <a href="./clock.html">view</a>
            </div>
          </div>
          <div className={`${styles["custom-grid"]}`}>
            <div className={`${styles["card"]}`}>
              <h6>Task Name</h6>
              <h4>Magic Ball</h4>
              <p>Creating magic ball application using HTML, CSS, jQuery</p>
              <a href="./magic-ball.html">view</a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
