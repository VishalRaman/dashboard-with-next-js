import React from "react";
import styles from "./header.module.scss";

export default function Headers() {
  return (
    <nav
      className={`${styles["nav-bar"]} flex justify-between align-center`}
      id="top-header"
    >
      <div className={`${styles["logo"]}`}>
        <a href="index.html">
          <img src="./logo.png" alt="logo" />
        </a>
      </div>
      <div className={`${styles["search-box"]}`} id="searchBox">
        <input
          type="text"
          id="focus"
          onclick="setFocus()"
          placeholder="Search..."
        />
        <button className={`${styles["close-btn"]} hide`}>X</button>
        <div className="hide" id="searchDropdown">
          <div className={`${styles["search-dropdown"]}`}>
            <a href="to-do.html" className={`${styles["title"]} flex`}>
              Todo List
            </a>
            <a href="clock.html" className={`${styles["title"]} flex`}>
              Clock
            </a>
            <a href="magic-ball.html" className={`${styles["title"]} flex`}>
              Magic Ball
            </a>
          </div>
        </div>
      </div>
      <div className={`${styles["user"]}  flex align-center`}>
        <a
          href="javasript:void(0);"
          className="text-center flex justify-center align-center"
        >
          <img src="./user.png" alt="" />
        </a>
        <p>Welcome Jane</p>
      </div>
    </nav>
  );
}
