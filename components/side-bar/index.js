import React from "react";
import Link from "next/link";
import styles from "./side-bar.module.scss";

export default function SideNav() {
  return (
    <div className={`${styles["side-nav"]}`} id="sideBar">
      <ul id="sideNavList">
        <li>
          <Link href="/">Dashboard</Link>
        </li>
        <li>
          <Link href="./to-do.html">To do list</Link>
        </li>
        <li>
          <Link href="./clock.html">Clock</Link>
        </li>
        <li>
          <Link href="./magic-ball.html">Magic ball</Link>
        </li>
        <li>
          <Link href="javascript:void(0);">Mouse Hover</Link>
        </li>
      </ul>
      <button
        type="button"
        className={`${styles["side-nav-btn"]}`}
        id="sideNavBtn"
      >
        X
      </button>
    </div>
  );
}
